import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import ToDo from './model/todo'
import schema from './graphql/Schema/Schema'
const app = express();

import {graphql} from 'graphql'
import graphqlHTTP from 'express-graphql';

app.use(bodyParser.json())

mongoose.connect('mongodb://localhost/Graphql')

var db = mongoose.connection;
db.on('error', ()=> {console.log( 'FAILED to connect to mongoose')})
db.once('open', () => {
	console.log( 'Connected to MongoDb')
})

app.listen(3000,()=> {console.log("Express Server is Running @ Port -> 3000!!!")})

app.get('/',(req,res)=>{
	res.sendFile(__dirname + '/index.html')
})

app.use('/graphql', graphqlHTTP (req => ({
	schema
	//,graphiql:true
})))

app.post('/quotes',(req,res)=>{
	// Insert into TodoList Collection
	console.log(req.body.item, "req.body.item");
	console.log(req.body.itemId, "req.body.item");
	console.log(req.body.completed, "req.body.item");

	var todoItem = new ToDo({
		itemId:req.body.itemId,
		item:req.body.item,
		completed: req.body.completed
	})

	todoItem.save((err,result)=> {
		if (err) {console.log("---TodoItem save failed " + err)}
		console.log("TodoItem saved successfully "+todoItem.item)
		res.status(200).end();
	})
})
